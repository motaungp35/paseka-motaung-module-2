//Define class
class App {
  var appName;
  var appCategory;
  var appDeveloper;
  var appYearWon;

  //Define class function and tranform the app name to all capital letters
  showAppinfo(){
    print("App name is : ${appName.toUpperCase()}");
    print("App category is : ${appCategory}");
    print("App developer is : ${appDeveloper}");
    print("Year app won MTN Business App of the year awards is : ${appYearWon}");
  }
}
void main(){

//Create object called App
var app = new App();
app.appName = "FNB App";
app.appCategory = "Finance App";
app.appDeveloper = "FirstRand Bank Limited";
app.appYearWon = "2012";

//Aceessing class function
app.showAppinfo();
}

