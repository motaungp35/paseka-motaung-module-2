void main(){

  var winningapps = ["FNB App","Shyft","SnapScan","WumDrop","takealot","CheckersSixty60","Pineapple","Naked Insurance","EskomSePush","Ambani Africa","Xitsoga Dictionary","Khula","StokFella"];

  //Sort and Print the apps
  winningapps.sort();
  print("Winning apps since 2012 : ${winningapps}");

  //Print the winning app of 2017
  print("Winning app of 2017 is : ${winningapps[7]}");

  //Print the winning app of 2018
  print("Winning app of 2018 is : ${winningapps[4]}");

  //Total number of apps from array
  print("Total number of apps from array is : ${winningapps.length}");

}